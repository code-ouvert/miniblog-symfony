# [MyBlog-Symfony](https://myblog-symfony.herokuapp.com/)

## About MyBlog and the developer

**MyBlog** is a coding blog where I publish artlcles related to HTML, CSS, JavaScript, PHP... as well as frameworks such as symfony, laravel... Here I also provide source codes for each of my [YouTube videos](https://www.youtube.com/c/CodeOuvert) and you can use these codes without any restriction or limitation. I believe my videos or codes help to inspire web designers and developers and also help to improve their skills.

My name is **Yassin El Kamal NGUESSU** and i am a **backend developer** at first self-taught then graduated with a bts in computer engineering. I have also worked on many **pfront end projects** in the past and I'm still working on it. Internet and web development is my passion and I believe in helping people with my abilities and knowledge. I've been learning these things since 2016 and I feel like learning is now part of my life.

## YouTube

[Code Ouvert](https://www.youtube.com/c/CodeOuvert)

## Website

[Open Code website link](https://myblog-symfony.herokuapp.com/)

## Goals

What are we going to see today? In this new project, I will code the website of **MyBlog**


## development tools

- A computer (accuracy is worth it)

- A text editor (Example: [Sublime Text](https://www.sublimetext.com/), [visual Studio Code](https://code.visualstudio.com/), ...)

- Three or more Browsers for testing (Google Chrome, Mozilla Firefox
, Microsoft Edge, ...)

- PHP 8.1

- Composer

- MySQL 8.0

- Symfony CLI

- git

You can check the prerequisite with the following command (from the Symfony CLI):

```bash
symfony book:check-requirements
symfony check:requirements
```

## displays project information

When working on an existing Symfony application for the first time, it may be useful to run this command which displays information about the project:

```bash
php bin/console about
```

## Launch the development environment

```bash
symfony serve -d
symfony open:local
npm run build
```

## Run tests

```bash
php bin/phpunit
```
Or to see the details
```bash
php bin/phpunit --testdox
```

## Purge HTTP cache for testing

```bash
symfony console cache:clear
```
You can manually remove all HTTP cache by deleting the var/cache/dev/http_cache/ directory:
```bash
rm -rf var/cache/dev/http_cache/
```

## Consume messages (run the Worker)

Once your messages are delivered, in most cases you will need to "consume" them. You can do this with the messenger:consume command:
```bash
php bin/console messenger:consume async
```
Or to see the details
```bash
php bin/console messenger:consume async -vv
```

## Resources

- [getbootstrap.com/](https://getbootstrap.com/)

- [Symfony.com](https://symfony.com/doc/6.0/the-fast-track/fr/index.html)
