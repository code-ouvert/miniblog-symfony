<?php

namespace App\Controller;

use Twig\Environment;
use App\Entity\Contact;
use App\Form\Contact\ContactFormType;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

#[Route('/contact')]
class ContactController extends AbstractController
{
    private $twig;

    public function __construct(
        Environment $twig,
    ) {
        $this->twig = $twig;
    }

    #[Route(
        '',
        name: 'app_contact',
        methods: ['GET', 'POST'],
    )]
    public function index(
        Request $request,
        ManagerRegistry $doctrine,
    ): Response {
        $em = $doctrine->getManager();

        $contact = new Contact;

        $contactForm = $this->createForm(ContactFormType::class, $contact);

        $contactForm->handleRequest($request);

        if ($contactForm->isSubmitted() && $contactForm->isValid())
        {
            $em->persist($contact);
            $em->flush();

            $this->addFlash('success', 'Thank you for your message. You will receive an answer within 48 hours maximum.');

            return $this->redirectToRoute('app_home');
        }

        if ($contactForm->isSubmitted()) {
            $this->addFlash('danger', 'Can you verify your submission? There are a few issues with this.');
        }

        $response = new Response($this->twig->render('contact/index.html.twig', [
            'contactForm' => $contactForm->createView(),
            'current_menu' => 'contact',
        ]));

        return $response;
    }
}
