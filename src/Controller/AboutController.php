<?php

namespace App\Controller;

use Twig\Environment;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

#[Route('/a-propos')]
class AboutController extends AbstractController
{
    private $twig;

    public function __construct(
        Environment $twig,
    ) {
        $this->twig = $twig;
    }

    #[Route(
        '',
        name: 'app_about',
        methods: ['GET'],
    )]
    public function index(): Response
    {
        $response = new Response($this->twig->render('about/index.html.twig', [
            'current_menu' => 'about',
        ]));

        //$response->setSharedMaxAge(3600);

        return $response;
    }
}
