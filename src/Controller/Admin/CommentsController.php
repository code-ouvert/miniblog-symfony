<?php

namespace App\Controller\Admin;

use Twig\Environment;
use App\Entity\Comment;
use App\Repository\CommentRepository;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

#[Route('/admin/comments')]
class CommentsController extends AbstractController
{
    private $twig;
    private $commentRepository;

    public function __construct(
        Environment $twig,
        CommentRepository $commentRepository,
    ) {
        $this->twig = $twig;
        $this->commentRepository = $commentRepository;
    }

    #[Route(
        '',
        name: 'app_admin_comments',
        methods: ['GET'],
    )]
    public function index(
        Request $request,
        PaginatorInterface $paginator,
    ): Response {
        $data = $comments = $this->commentRepository->findAllComments();

        $nbrComments = count($comments);

        $comments = $paginator->paginate(
           $data, /**query NOT result*/
           $request->query->getInt('page', 1), /*page nomber*/
           20 /*limit per page*/
        );

        return $this->render('admin/comments/index.html.twig', [
            'comments' => $comments,
            'nbrComments' => $nbrComments,
            'current_menu' => 'comments',
        ]);
    }

    #[Route(
        '/is-verified/{id<[0-9]+>}',
        name: 'app_admin_comments_is_verified',
        methods: ['GET'],
        requirements: ['id' => '\d+'],
    )]
    public function isVerified(
        Request $request,
        ManagerRegistry $doctrine,
        Comment $comment,
    ): Response {
        if ($this->isCsrfTokenValid('comment_is_verified_' . $comment->getId(), $request->query->get('csrf_token'))) {

            $em = $doctrine->getManager();

            if ($comment->getIsVerified() == false) {

                $comment->setIsVerified(true);

                $em->flush();
            } else {
                $comment->setIsVerified(false);
                $em->flush();
            }
        } else {
            $this->addFlash('danger', 'Error: Comment no modified. Token invalide!');
        }

        return $this->redirectToRoute('app_admin_comments');
    }

    #[Route(
        '/delete/{id<[0-9]+>}',
        name: 'app_admin_comments_delete',
        methods: ['GET'],
        requirements: ['id' => '\d+'],
    )]
    public function delete(
        Comment $comment,
        Request $request,
        ManagerRegistry $doctrine,
    ): Response {
        $em = $doctrine->getManager();
    
        if ($this->isCsrfTokenValid('comment_deletion_' . $comment->getId(), $request->query->get('csrf_token'))) {

            $em->remove($comment);
            $em->flush();

            $this->addFlash('success', 'Comment DELETED successfully');
        }
        else{
            $this->addFlash('danger', 'Error: Comment no Delete. Token invalide!');
        }

        return $this->redirectToRoute('app_admin_comments');
    }
}
