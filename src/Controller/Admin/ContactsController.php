<?php

namespace App\Controller\Admin;

use Twig\Environment;
use App\Repository\ContactRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

#[Route('/admin/contacts')]
class ContactsController extends AbstractController
{
    private $twig;
    private $contactRepository;

    public function __construct(
        Environment $twig,
        ContactRepository $contactRepository,
    ) {
        $this->twig = $twig;
        $this->contactRepository = $contactRepository;
    }

    #[Route(
        '',
        name: 'app_admin_contacts',
        methods: ['GET'],
    )]
    public function index(
        Request $request,
        PaginatorInterface $paginator,
    ): Response {
        $data = $contacts = $this->contactRepository->findAllContacts();

        $nbrContacts = count($contacts);

        $contacts = $paginator->paginate(
           $data, /**query NOT result*/
           $request->query->getInt('page', 1), /*page nomber*/
           20 /*limit per page*/
        );

        return $this->render('admin/contacts/index.html.twig', [
            'contacts' => $contacts,
            'nbrContacts' => $nbrContacts,
            'current_menu' => 'contacts',
        ]);
    }
}
