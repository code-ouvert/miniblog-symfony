<?php

namespace App\Controller\Admin;

use Twig\Environment;
use App\Entity\Article;
use App\Repository\ArticleRepository;
use App\Form\Article\ArticleFormType;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

#[Route('/admin')]
class ArticlesController extends AbstractController
{
    private $twig;
    private $articleRepository;

    public function __construct(
        Environment $twig,
        ArticleRepository $articleRepository,
    ) {
        $this->twig = $twig;
        $this->articleRepository = $articleRepository;
    }

    #[Route(
        '',
        name: 'app_admin',
        methods: ['GET'],
    )]
    public function index(
        Request $request,
        PaginatorInterface $paginator,
    ): Response {
        $data = $articles = $this->articleRepository->findAllArticles();

        $nbrArticles = count($articles);

        $articles = $paginator->paginate(
           $data, /**query NOT result*/
           $request->query->getInt('page', 1), /*page nomber*/
           20 /*limit per page*/
        );

        return $this->render('admin/articles/index.html.twig', [
            'articles' => $articles,
            'nbrArticles' => $nbrArticles,
            'current_menu' => 'articles',
        ]);
    }

    #[Route(
        '/articles/create',
        name: 'app_admin_articles_create',
        methods: ['GET', 'POST'],
    )]
    public function create(
        Request $request,
        ManagerRegistry $doctrine,
    ): Response {
        $em = $doctrine->getManager();

        $article = new Article;

        $form = $this->createForm(ArticleFormType::class, $article);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $article->setUser($this->getUser());

            $em->persist($article);
            $em->flush();

            $this->addFlash('success', 'Article CREATED successfully');

            return $this->redirectToRoute('app_admin');
        }

        if ($form->isSubmitted()) {
            $this->addFlash('danger', 'Can you verify your submission? There are a few issues with this.');
        }

        return $this->render('admin/articles/create.html.twig', [
            'form' => $form->createView(),
            'current_menu' => 'articles',
        ]);
    }

    #[Route(
        '/articles/edit/{slug}-{id<[0-9]+>}',
        name: 'app_admin_articles_edit',
        methods: ['GET', 'POST'],
        requirements: ['slug' => '[a-z0-9\-]*'],
    )]
    public function edit(
        string $slug,
        Article $article,
        Request $request,
        ManagerRegistry $doctrine,
    ): Response {
        if ($article->getSlug() !== $slug) {
            return $this->redirectToRoute('app_admin_articles_edit', [
                'id' => $article->getId(),
                'slug' => $article->getSlug(),
            ], 301);
        }

        $em = $doctrine->getManager();

        $form = $this->createForm(ArticleFormType::class, $article);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $em->flush();

            $this->addFlash('success', 'Article EDITED successfully');

            return $this->redirectToRoute('app_admin');
        }

        if ($form->isSubmitted()) {
            $this->addFlash('danger', 'Can you verify your submission? There are a few issues with this.');
        }

        return $this->render('admin/articles/edit.html.twig', [
            'article' => $article,
            'form' => $form->createView(),
            'current_menu' => 'articles',
        ]);
    }

    #[Route(
        '/articles/is-verified/{id<[0-9]+>}',
        name: 'app_admin_articles_is_verified',
        methods: ['GET'],
        requirements: ['id' => '\d+'],
    )]
    public function isVerified(
        Request $request,
        ManagerRegistry $doctrine,
        Article $article,
    ): Response {
        if ($this->isCsrfTokenValid('article_is_verified_' . $article->getId(), $request->query->get('csrf_token'))) {

            $em = $doctrine->getManager();

            if ($article->getIsVerified() == false) {

                $article->setIsVerified(true);

                $em->flush();
            } else {
                $article->setIsVerified(false);
                $em->flush();
            }
        } else {
            $this->addFlash('danger', 'Error: Article no modified. Token invalide!');
        }

        return $this->redirectToRoute('app_admin');
    }

    #[Route(
        '/articles/delete/{id<[0-9]+>}',
        name: 'app_admin_articles_delete',
        methods: ['GET'],
        requirements: ['id' => '\d+'],
    )]
    public function delete(
        Article $article,
        Request $request,
        ManagerRegistry $doctrine,
    ): Response {
        $em = $doctrine->getManager();
    
        if ($this->isCsrfTokenValid('article_deletion_' . $article->getId(), $request->query->get('csrf_token'))) {

            $em->remove($article);
            $em->flush();

            $this->addFlash('success', 'Article DELETED successfully');
        }
        else{
            $this->addFlash('danger', 'Error: Article no Delete. Token invalide!');
        }

        return $this->redirectToRoute('app_admin');
    }
}
