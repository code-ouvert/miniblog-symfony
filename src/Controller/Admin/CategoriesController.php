<?php

namespace App\Controller\Admin;

use Twig\Environment;
use App\Entity\Category;
use App\Repository\CategoryRepository;
use App\Form\Category\CategoryFormType;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

#[Route('/admin/categories')]
class CategoriesController extends AbstractController
{
    private $twig;
    private $categoryRepository;

    public function __construct(
        Environment $twig,
        CategoryRepository $categoryRepository,
    ) {
        $this->twig = $twig;
        $this->categoryRepository = $categoryRepository;
    }

    #[Route(
        '',
        name: 'app_admin_categories',
        methods: ['GET', 'POST'],
    )]
    public function index(
        Request $request,
        PaginatorInterface $paginator,
    ): Response {
        $data = $categories = $this->categoryRepository->findAllCategories();

        $nbrCategories = count($categories);

        $categories = $paginator->paginate(
           $data, /**query NOT result*/
           $request->query->getInt('page', 1), /*page nomber*/
           20 /*limit per page*/
        );
        
        return $this->render('admin/categories/index.html.twig', [
            'categories' => $categories,
            'nbrCategories' => $nbrCategories,
            'current_menu' => 'categories',
        ]);
    }

    #[Route(
        '/create',
        name: 'app_admin_categories_create',
        methods: ['GET', 'POST'],
    )]
    public function create(
        Request $request,
        ManagerRegistry $doctrine
    ): Response {
        $em = $doctrine->getManager();

        $category = new Category;

        $form = $this->createForm(CategoryFormType::class, $category);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $em->persist($category);
            $em->flush();

            $this->addFlash('success', 'Category CREATED successfully');

            return $this->redirectToRoute('app_admin_categories');
        }

        if ($form->isSubmitted()) {
            $this->addFlash('danger', 'Can you verify your submission? There are a few issues with this.');
        }
        
        return $this->render('admin/categories/create.html.twig', [
            'form' => $form->createView(),
            'current_menu' => 'categories',
        ]);
    }

    #[Route(
        '/edit/{slug}-{id<[0-9]+>}',
        name: 'app_admin_categories_edit',
        methods: ['GET', 'POST'],
        requirements: ['slug' => '[a-z0-9\-]*'],
    )]
    public function edit(
        string $slug,
        Request $request,
        Category $category,
        ManagerRegistry $doctrine,
    ): Response {
        if ($category->getSlug() !== $slug) {
            return $this->redirectToRoute('app_admin_categories_edit', [
                'id' => $category->getId(),
                'slug' => $category->getSlug(),
            ], 301);
        }
        
        $em = $doctrine->getManager();

        $form = $this->createForm(CategoryFormType::class, $category);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $em->flush();

            $this->addFlash('success', 'Category EDITED successfully');

            return $this->redirectToRoute('app_admin_categories');
        }

        if ($form->isSubmitted()) {
            $this->addFlash('danger', 'Can you verify your submission? There are a few issues with this.');
        }
        
        return $this->render('admin/categories/edit.html.twig', [
            'category' => $category,
            'form' => $form->createView(),
            'current_menu' => 'categories',
        ]);
    }

    #[Route(
        '/delete/{id<[0-9]+>}',
        name: 'app_admin_categories_delete',
        methods: ['GET'],
        requirements: ['id' => '\d+'],
    )]
    public function delete(
        Category $category,
        Request $request,
        ManagerRegistry $doctrine,
    ): Response {
        $em = $doctrine->getManager();
    
        if ($this->isCsrfTokenValid('category_deletion_' . $category->getId(), $request->query->get('csrf_token'))) {

            $em->remove($category);
            $em->flush();

            $this->addFlash('success', 'Category DELETED successfully');
        }
        else{
            $this->addFlash('danger', 'Error: Category no Delete. Token invalide!');
        }

        return $this->redirectToRoute('app_admin_categories');
    }
}
