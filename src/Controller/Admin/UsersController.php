<?php

namespace App\Controller\Admin;

use App\Entity\User;
use Twig\Environment;
use App\Form\User\UserFormType;
use App\Repository\UserRepository;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

#[Route('/admin/users')]
class UsersController extends AbstractController
{
    private $twig;
    private $userRepository;

    public function __construct(
        Environment $twig,
        UserRepository $userRepository,
    ) {
        $this->twig = $twig;
        $this->userRepository = $userRepository;
    }

    #[Route(
        '',
        name: 'app_admin_users',
        methods: ['GET'],
    )]
    public function index(
        Request $request,
        PaginatorInterface $paginator,
    ): Response {
        $data = $users = $this->userRepository->findAllUsers();

        $nbrUsers = count($users);

        $users = $paginator->paginate(
           $data, /**query NOT result*/
           $request->query->getInt('page', 1), /*page nomber*/
           20 /*limit per page*/
        );

        return $this->render('admin/users/index.html.twig', [
            'users' => $users,
            'nbrUsers' => $nbrUsers,
            'current_menu' => 'users',
        ]);
    }

    #[Route(
        '/create',
        name: 'app_admin_users_create',
        methods: ['GET', 'POST'],
    )]
    #[Security("is_granted('ROLE_ADMIN')")]
    #[IsGranted("IS_AUTHENTICATED_FULLY")]
    public function create(
        Request $request,
        ManagerRegistry $doctrine,
        UserPasswordHasherInterface $userPasswordHasher,
    ): Response {
        $em = $doctrine->getManager();

        $user = new User;

        $form = $this->createForm(UserFormType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $user->setPassword(
            $userPasswordHasher->hashPassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );

            $em->persist($user);
            $em->flush();

            $this->addFlash('success', 'User CREATED successfully');

            return $this->redirectToRoute('app_admin_users');
        }

        if ($form->isSubmitted()) {
            $this->addFlash('danger', 'Can you verify your submission? There are a few issues with this.');
        }
        
        return $this->render('admin/users/create.html.twig', [
            'form' => $form->createView(),
            'current_menu' => 'users',
        ]);
    }

    #[Route(
        '/edit/{slug}-{id<[0-9]+>}',
        name: 'app_admin_users_edit',
        methods: ['GET', 'POST'],
        requirements: ['slug' => '[a-z0-9\-]*']
    )]
    #[Security("is_granted('ROLE_ADMIN')")]
    #[IsGranted("IS_AUTHENTICATED_FULLY")]
    public function edit(
        User $user,
        string $slug,
        Request $request,
        ManagerRegistry $doctrine,
        UserPasswordHasherInterface $userPasswordHasher,
    ): Response {
        if ($user->getSlug() !== $slug) {
            return $this->redirectToRoute('app_admin_users_edit', [
                'id' => $user->getId(),
                'slug' => $user->getSlug(),
            ], 301);
        }

        $em = $doctrine->getManager();

        $form = $this->createForm(UserFormType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $user->setPassword(
            $userPasswordHasher->hashPassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );
            
            $em->flush();

            $this->addFlash('success', 'User EDITED successfully');

            return $this->redirectToRoute('app_admin_users');
        }

        if ($form->isSubmitted()) {
            $this->addFlash('danger', 'Can you verify your submission? There are a few issues with this.');
        }
        
        return $this->render('admin/users/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
            'current_menu' => 'users',
        ]);
    }

    #[Route(
        '/delete/{id<[0-9]+>}',
        name: 'app_admin_users_delete',
        methods: ['GET'],
        requirements: ['id' => '\d+'],
    )]
    #[Security("is_granted('ROLE_ADMIN')")]
    #[IsGranted("IS_AUTHENTICATED_FULLY")]
    public function delete(
        User $user,
        Request $request,
        ManagerRegistry $doctrine,
    ): Response {
        $em = $doctrine->getManager();
    
        if ($this->isCsrfTokenValid('user_deletion_' . $user->getId(), $request->query->get('csrf_token'))) {

            $em->remove($user);
            $em->flush();

            $this->addFlash('success', 'User DELETED successfully');
        }
        else{
            $this->addFlash('danger', 'Error: User no Delete. Token invalide!');
        }

        return $this->redirectToRoute('app_admin_users');
    }
}
