<?php

namespace App\Controller\Admin;

use App\Entity\Tags;
use Twig\Environment;
use App\Form\Tags\TagsFormType;
use App\Repository\TagsRepository;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

#[Route('/admin/tags')]
class TagsController extends AbstractController
{
    private $twig;
    private $tagsRepository;

    public function __construct(
        Environment $twig,
        TagsRepository $tagsRepository,
    ) {
        $this->twig = $twig;
        $this->tagsRepository = $tagsRepository;
    }

    #[Route(
        '',
        name: 'app_admin_tags',
        methods: ['GET'],
    )]
    public function index(
        Request $request,
        PaginatorInterface $paginator,
    ): Response {
        $data = $tags = $this->tagsRepository->findAllTags();

        $nbrTags = count($tags);

        $tags = $paginator->paginate(
           $data, /**query NOT result*/
           $request->query->getInt('page', 1), /*page nomber*/
           20 /*limit per page*/
        );

        return $this->render('admin/tags/index.html.twig', [
            'tags' => $tags,
            'nbrTags' => $nbrTags,
            'current_menu' => 'tags',
        ]);
    }

    #[Route(
        '/create',
        name: 'app_admin_tags_create',
        methods: ['GET', 'POST'],
    )]
    public function create(
        Request $request,
        ManagerRegistry $doctrine,
    ): Response {
        $em = $doctrine->getManager();

        $tags = new Tags;

        $form = $this->createForm(TagsFormType::class, $tags);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $em->persist($tags);
            $em->flush();

            $this->addFlash('success', 'Tags CREATED successfully');

            return $this->redirectToRoute('app_admin_tags');
        }

        if ($form->isSubmitted()) {
            $this->addFlash('danger', 'Can you verify your submission? There are a few issues with this.');
        }
        
        return $this->render('admin/tags/create.html.twig', [
            'form' => $form->createView(),
            'current_menu' => 'tags',
        ]);
    }

    #[Route(
        '/edit/{slug}-{id<[0-9]+>}',
        name: 'app_admin_tags_edit',
        methods: ['GET', 'POST'],
        requirements: ['slug' => '[a-z0-9\-]*']
    )]
    public function edit(
        Tags $tags,
        string $slug,
        Request $request,
        ManagerRegistry $doctrine,
    ): Response {
        if ($tags->getSlug() !== $slug) {
            return $this->redirectToRoute('app_admin_tags_edit', [
                'id' => $tags->getId(),
                'slug' => $tags->getSlug(),
            ], 301);
        }

        $em = $doctrine->getManager();

        $form = $this->createForm(TagsFormType::class, $tags);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $em->flush();

            $this->addFlash('success', 'Tags EDITED successfully');

            return $this->redirectToRoute('app_admin_tags');
        }

        if ($form->isSubmitted()) {
            $this->addFlash('danger', 'Can you verify your submission? There are a few issues with this.');
        }
        
        return $this->render('admin/tags/edit.html.twig', [
            'tags' => $tags,
            'form' => $form->createView(),
            'current_menu' => 'tags',
        ]);
    }

    #[Route(
        '/delete/{id<[0-9]+>}',
        name: 'app_admin_tags_delete',
        methods: ['GET'],
        requirements: ['id' => '\d+'],
    )]
    public function delete(
        Tags $tags,
        Request $request,
        ManagerRegistry $doctrine,
    ): Response {
        $em = $doctrine->getManager();
    
        if ($this->isCsrfTokenValid('tags_deletion_' . $tags->getId(), $request->query->get('csrf_token'))) {

            $em->remove($tags);
            $em->flush();

            $this->addFlash('success', 'Tags DELETED successfully');
        }
        else{
            $this->addFlash('danger', 'Error: Tags no Delete. Token invalide!');
        }

        return $this->redirectToRoute('app_admin_tags');
    }
}
