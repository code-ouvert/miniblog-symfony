<?php

namespace App\Controller;

use App\Entity\Tags;
use Twig\Environment;
use App\Entity\Comment;
use App\Entity\Article;
use App\Entity\Category;
use App\Repository\TagsRepository;
use App\Repository\ArticleRepository;
use App\Repository\CommentRepository;
use App\Form\Comment\CommentFormType;
use App\Repository\CategoryRepository;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

#[Route('')]
class ArticlesController extends AbstractController
{
    private $twig;
    private $tagsRepository;
    private $articleRepository;
    private $commentRepository;
    private $categoryRepository;

    public function __construct(
        Environment $twig,
        TagsRepository $tagsRepository,
        ArticleRepository $articleRepository,
        CommentRepository $commentRepository,
        CategoryRepository $categoryRepository,
    ) {
        $this->twig = $twig;
        $this->tagsRepository = $tagsRepository;
        $this->articleRepository = $articleRepository;
        $this->commentRepository = $commentRepository;
        $this->categoryRepository = $categoryRepository;
    }

    #[Route(
        '',
        name: 'app_home',
        methods: ['GET'],
    )]
    public function index(
        PaginatorInterface $paginator,
        Request $request,
    ): Response {
        $tags = $this->tagsRepository->findAllTags();

        $categories = $this->categoryRepository->findAllCategories();

        $data = $articles = $this->articleRepository->findAllVisibleArticles();

        $nbrArticles = count($articles);

        $articles = $paginator->paginate(
           $data, /**query NOT result*/
           $request->query->getInt('page', 1), /*page nomber*/
           10 /*limit per page*/
        );

        $response = new Response($this->twig->render('articles/index.html.twig', [
            'tags' => $tags,
            'categories' => $categories,
            'articles' => $articles,
            'nbrArticles' => $nbrArticles,
            'current_menu' => 'home',
        ]));

        //$response->setSharedMaxAge(3600);

        return $response;
    }

    #[Route(
        '/{slug}-{id<[0-9]+>}',
        name: 'app_articles_show',
        methods: ['GET', 'POST'],
        requirements: ['slug' => '[a-z0-9\-]*'],
    )]
    public function show(
        Article $article,
        Request $request,
        ManagerRegistry $doctrine,
        string $slug,
    ): Response {
        if ($article->getSlug() !== $slug) {
            return $this->redirectToRoute('app_articles_show', [
                'id' => $article->getId(),
                'slug' => $article->getSlug(),
            ], 301);
        }

        $tags = $this->tagsRepository->findAllTags();

        $categories = $this->categoryRepository->findAllCategories();

        $article_id = $article->getId();

        $comments = $this->commentRepository->findAllVisiblesCommentsByArticle($article_id);

        $nbrComments = count($comments);


        $em = $doctrine->getManager();

        $comment = new Comment;

        $comment->setArticle($article);
        
        $formComment = $this->createForm(CommentFormType::class, $comment);

        $formComment->handleRequest($request);

        if ($formComment->isSubmitted() && $formComment->isValid())
        {
            $em->persist($comment);
            $em->flush();

            $this->addFlash('success', 'Thank you. Your comment successfully send!');

            return $this->redirectToRoute('app_articles_show', [
                'id' => $article->getId(),
                'slug' => $article->getSlug(),
            ]);
        }

        if ($formComment->isSubmitted()) {
            $this->addFlash('danger', 'Can you verify your submission? There are a few issues with this.');
        }

        $response = new Response($this->twig->render('articles/show.html.twig', [
            'article' => $article,
            'tags' => $tags,
            'categories' => $categories,
            'comments' => $comments,
            'nbrComments' => $nbrComments,
            'formComment' => $formComment->createView(),
            'current_menu' => 'home',
        ]));

        //$response->setSharedMaxAge(3600);

        return $response;
    }

    #[Route(
        '/categories/{slug}-{id<[0-9]+>}',
        name: 'app_articles_categories',
        methods: ['GET'],
        requirements: ['slug' => '[a-z0-9\-]*'],
    )]
    public function category(
        Category $category,
        PaginatorInterface $paginator,
        Request $request,
        string $slug,
    ): Response {
        if ($category->getSlug() !== $slug) {
            return $this->redirectToRoute('app_articles_categories', [
                'id' => $category->getId(),
                'slug' => $category->getSlug(),
            ], 301);
        }
        $tags = $this->tagsRepository->findAllTags();

        $categories = $this->categoryRepository->findAllCategories();

        $category_id = $category->getId();

        $data = $articles = $this->articleRepository->findAllVisiblesArticlesByCategory($category_id);

        $nbrArticles = count($articles);

        $articles = $paginator->paginate(
           $data, /**query NOT result*/
           $request->query->getInt('page', 1), /*page nomber*/
           10 /*limit per page*/
        );

        $response = new Response($this->twig->render('articles/categories.html.twig', [
            'category' => $category,
            'tags' => $tags,
            'categories' => $categories,
            'articles' => $articles,
            'nbrArticles' => $nbrArticles,
            'current_menu' => 'home',
        ]));

        //$response->setSharedMaxAge(3600);

        return $response;
    }

    #[Route(
        '/tags/{slug}-{id<[0-9]+>}',
        name: 'app_articles_tags',
        methods: ['GET'],
        requirements: ['slug' => '[a-z0-9\-]*'],
    )]
    public function tag(
        Tags $tag,
        PaginatorInterface $paginator,
        Request $request,
        string $slug,
    ): Response {
        if ($tag->getSlug() !== $slug) {
            return $this->redirectToRoute('app_articles_tags', [
                'id' => $tag->getId(),
                'slug' => $tag->getSlug(),
            ], 301);
        }
        $tags = $this->tagsRepository->findAllTags();

        $categories = $this->categoryRepository->findAllCategories();

        $tag_id = $tag->getId();

        $data = $articles = $this->articleRepository->findAllVisiblesArticlesByTag($tag_id);

        $nbrArticles = count($articles);

        $articles = $paginator->paginate(
           $data, /**query NOT result*/
           $request->query->getInt('page', 1), /*page nomber*/
           10 /*limit per page*/
        );

        $response = new Response($this->twig->render('articles/tags.html.twig', [
            'tag' => $tag,
            'tags' => $tags,
            'categories' => $categories,
            'articles' => $articles,
            'nbrArticles' => $nbrArticles,
            'current_menu' => 'home',
        ]));

        //$response->setSharedMaxAge(3600);

        return $response;
    }
}
