<?php

namespace App\Repository;

use App\Entity\Article;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Article>
 *
 * @method Article|null find($id, $lockMode = null, $lockVersion = null)
 * @method Article|null findOneBy(array $criteria, array $orderBy = null)
 * @method Article[]    findAll()
 * @method Article[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Article::class);
    }

    public function add(Article $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Article $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @return Article[] Returns an array of Article objects
     */
    public function findAllArticles()
    {
        $query = $this->createQueryBuilder('a')
            ->orderBy('a.createdAt', 'DESC')
        ;
        return $query->getQuery()->getResult();
    }

    /**
     * @return Article[] Returns an array of Article objects
     */
    public function findAllVisibleArticles()
    {
        $isVerified = true;

        $query = $this->createQueryBuilder('a')
            ->andWhere('a.isVerified = :val')
            ->setParameter('val', $isVerified)
            ->orderBy('a.createdAt', 'DESC')
        ;
        return $query->getQuery()->getResult();
    }

    /**
     * @return Article[] Returns an array of Article objects
     */
    public function findAllVisiblesArticlesByCategory(int $category_id)
    {
        $isVerified = true;

        $query = $this->createQueryBuilder('a')
            ->select('c', 'a')
            ->join('a.category', 'c')
            ->andWhere('c.id = :val1')
            ->setParameter('val1', $category_id)
            ->andWhere('a.isVerified = :val2')
            ->setParameter('val2', $isVerified)
            ->orderBy('a.createdAt', 'DESC')
        ;
        return $query->getQuery()->getResult();
    }

    /**
     * @return Article[] Returns an array of Article objects
     */
    public function findAllVisiblesArticlesByTag(int $tag_id)
    {
        $isVerified = true;

        $query = $this->createQueryBuilder('a')
            ->select('t', 'a')
            ->join('a.tags', 't')
            ->andWhere('t.id = :val1')
            ->setParameter('val1', $tag_id)
            ->andWhere('a.isVerified = :val2')
            ->setParameter('val2', $isVerified)
            ->orderBy('a.createdAt', 'DESC')
        ;
        return $query->getQuery()->getResult();
    }

//    /**
//     * @return Article[] Returns an array of Article objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('a')
//            ->andWhere('a.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('a.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Article
//    {
//        return $this->createQueryBuilder('a')
//            ->andWhere('a.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
