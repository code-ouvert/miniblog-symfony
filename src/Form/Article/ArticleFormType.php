<?php

namespace App\Form\Article;

use App\Entity\Tags;
use App\Entity\Article;
use App\Entity\Category;
use Symfony\Component\Form\AbstractType;
//use FOS\CKEditorBundle\Form\Type\CKEditorType;
Use Vich\UploaderBundle\Form\Type\VichImageType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class ArticleFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', TextType::class, [
                'label' => 'Title'
            ])
            ->add('imageFile', VichImageType::class, [
                'label' => 'Image (JPD or PNG file)',
                'required' => false,
                'allow_delete' => true,
                'download_uri' => false,
                'imagine_pattern' => 'squared_thumbnail_article_small',
            ])
            ->add('content', TextareaType::class, [
                'label' => 'Article Contents',
            ])
            // ->add('content', CKEditorType::class, [
            //     'label' => 'Article Contents',
            // ])
            ->add('tags', EntityType::class, [
                'class' => Tags::class,
                'choice_label' => 'name',
                'required' => false,
                'multiple' => true
            ])
            ->add('category', EntityType::class, [
                'class' => Category::class,
                'choice_label' => 'name',
            ])  
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Article::class,
        ]);
    }
}
